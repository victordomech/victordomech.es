import React, { Component } from "react";
import Helmet from "../component/common/Helmet";
import {FaTwitter ,FaInstagram , FaLinkedinIn} from "react-icons/fa";
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp } from "react-icons/fi";
import HeaderThree from "../component/header/HeaderThree";
import FooterTwo from "../component/footer/FooterTwo";
import Img from "react-webp-image";

const SocialShare = [
    {Social: <FaLinkedinIn /> , link: 'https://es.linkedin.com/in/victor-domenech'},
    {Social: <FaInstagram /> , link: 'https://www.instagram.com/vdrvictor/'},
    {Social: <FaTwitter /> , link: 'https://twitter.com/domenech_epa'},
]

class ProjectBarnaPuntCar extends Component{
    render(){
        return(
            <div className="active-dark">
                <React.Fragment>
                    <Helmet pageTitle="Victor - Proyecto BarnapuntCar" />

                    <HeaderThree homeLink="/" logo="symbol-dark" color="color-black"/>
                    
                    {/* Start Breadcrump Area */}
                    <div className="rn-page-title-area pt--120 pb--190 bg_image bg_image--project bg_image--barnapuntcar"  data-black-overlay="7">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="rn-page-title text-center pt--100">
                                        <h2 className="title theme-gradient">BarnaPuntCar</h2>
                                        <p>Taller mecánico con gestión de usuarios, estilo ordenado y garantizando seriedad.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* End Breadcrump Area */}

                    {/* Start Portfolio Details */}
                    <div className="rn-portfolio-details ptb--120 bg_color--1">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="portfolio-details">
                                        <div className="inner">
                                            <h2>Taller BarnaPuntCar</h2>
                                            <p className="subtitle">Centro de mantenimiento de coches y motos multimarca, venta de recambios, neumáticos y accesorios de coches y motos con la mejor relación calidad-precio.</p>

                                            <div className="portfolio-view-list d-flex flex-wrap">
                                            <div className="port-view">
                                                    <span>Tipo de proyecto</span>
                                                    <h4>Web</h4>
                                                </div>

                                                <div className="port-view">
                                                    <span>Tecnología</span>
                                                    <h4>PHP</h4>
                                                </div>

                                                <div className="port-view">
                                                    <span>Duración</span>
                                                    <h4>5 meses</h4>
                                                </div>
                                            </div>

                                            <div className="portfolio-share-link mt--20 pb--70 pb_sm--40">
                                                <ul className="social-share rn-lg-size d-flex justify-content-start liststyle mt--15">
                                                    {SocialShare.map((val , i) => (
                                                        <li key={i}><a href={`${val.link}`}>{val.Social}</a></li>
                                                    ))}
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="portfolio-thumb-inner inner">

                                            <div className="thumb mb--30">
                                                <Img className="w-100" src="/assets/images/victor/BarnaPuntCar.jpg" webp="/assets/images/victor/BarnaPuntCar.webp" alt="Proyecto BarnapPuntCar" />
                                            </div>

                                            <div className="thumb mb--30">
                                                <Img className="w-100" src="/assets/images/victor/ProjectDetails/Detail-BarnaPuntCar1.png" webp="/assets/images/victor/ProjectDetails/Detail-BarnaPuntCar1.webp" alt="Proyecto BarnapPuntCar" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* End Portfolio Details */}

                    {/* Start Back To Top */}
                    <div className="backto-top">
                        <ScrollToTop showUnder={160}>
                            <FiChevronUp />
                        </ScrollToTop>
                    </div>
                    {/* End Back To Top */}
                    
                    <FooterTwo />


                </React.Fragment>
            </div>
        )
    }
}
export default ProjectBarnaPuntCar;
