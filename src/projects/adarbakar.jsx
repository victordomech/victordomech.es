import React, { Component } from "react";
import Helmet from "../component/common/Helmet";
import {FaTwitter ,FaInstagram , FaLinkedinIn} from "react-icons/fa";
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp } from "react-icons/fi";
import HeaderThree from "../component/header/HeaderThree";
import FooterTwo from "../component/footer/FooterTwo";
import Img from "react-webp-image";

const SocialShare = [
    {Social: <FaLinkedinIn /> , link: 'https://es.linkedin.com/in/victor-domenech'},
    {Social: <FaInstagram /> , link: 'https://www.instagram.com/vdrvictor/'},
    {Social: <FaTwitter /> , link: 'https://twitter.com/domenech_epa'},
]

class ProjectAdarbakar extends Component{
    render(){
        return(
            <div className="active-dark">
                <React.Fragment>
                    <Helmet pageTitle="Victor - Proyecto Adarbakar" />

                    <HeaderThree homeLink="/" logo="symbol-dark" color="color-black"/>
                    
                    {/* Start Breadcrump Area */}
                    <div className="rn-page-title-area pt--120 pb--190 bg_image bg_image--project bg_image--adarbakar"  data-black-overlay="7">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="rn-page-title text-center pt--100">
                                        <h2 className="title theme-gradient">Adarbakar</h2>
                                        <p>Tienda online pensada para una alta conversión de venta con un diseño muy cuidado</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* End Breadcrump Area */}

                    {/* Start Portfolio Details */}
                    <div className="rn-portfolio-details ptb--120 bg_color--1">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="portfolio-details">
                                        <div className="inner">
                                            <h2>Ecommerce Adarbakar</h2>
                                            <p className="subtitle">Adarbakar es una marca artística que une cerámica, fotografía y poesía. <br></br>Adarbakar es más que una marca, es un intento de crear belleza y trasmitirla, de mostrar mi obsesión por los árboles, por la tierra, por la perfección que nos muestra la naturaleza </p>

                                            <div className="portfolio-view-list d-flex flex-wrap">

                                                <div className="port-view">
                                                    <span>Tipo de proyecto</span>
                                                    <h4>Tienda online</h4>
                                                </div>

                                                <div className="port-view">
                                                    <span>Tecnología</span>
                                                    <h4>Shopify</h4>
                                                </div>

                                                <div className="port-view">
                                                    <span>Duración</span>
                                                    <h4>2 meses</h4>
                                                </div>
                                            </div>

                                            <div className="portfolio-share-link mt--20 pb--70 pb_sm--40">
                                                <ul className="social-share rn-lg-size d-flex justify-content-start liststyle mt--15">
                                                    {SocialShare.map((val , i) => (
                                                        <li key={i}><a href={`${val.link}`}>{val.Social}</a></li>
                                                    ))}
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="portfolio-thumb-inner inner">

                                            <div className="thumb mb--30">
                                                <Img className="w-100" src="/assets/images/victor/Adarbakar.jpg" webp="/assets/images/victor/Adarbakar.webp" alt="Proyecto Adarbakar" />
                                            </div>

                                            <div className="thumb mb--30">
                                                <Img className="w-100" src="/assets/images/victor/ProjectDetails/Detail-Adarbakar1.png" webp="/assets/images/victor/ProjectDetails/Detail-Adarbakar1.webp" alt="Proyecto Adarbakar" />
                                            </div>

                                            <div className="thumb mb--30">
                                                <Img className="w-100" src="/assets/images/victor/ProjectDetails/Detail-Adarbakar2.png" webp="/assets/images/victor/ProjectDetails/Detail-Adarbakar2.webp" alt="Proyecto Adarbakar" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* End Portfolio Details */}

                    {/* Start Back To Top */}
                    <div className="backto-top">
                        <ScrollToTop showUnder={160}>
                            <FiChevronUp />
                        </ScrollToTop>
                    </div>
                    {/* End Back To Top */}
                    
                    <FooterTwo />


                </React.Fragment>
            </div>
        )
    }
}
export default ProjectAdarbakar;
