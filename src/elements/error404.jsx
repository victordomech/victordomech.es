import React, { Component } from 'react';
import HeaderThree from "../component/header/HeaderThree";
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp } from "react-icons/fi";
import FooterTwo from "../component/footer/FooterTwo";

 class error404 extends Component {
    
    render() {
        return (
            <div className="active-dark">
                <HeaderThree homeLink="/" logo="symbol-dark" color="color-black"/>
                {/* Start Page Error  */}
                <div className="error-page-inner bg_color--4">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="inner">
                                    <h1 className="title theme-gradient">404!</h1>
                                    <h3 className="sub-title">Page not found</h3>
                                    <span>No se pudo encontrar la página que estaba buscando.</span>
                                    <div className="error-button">
                                        <a className="rn-button-style--2 btn-solid" href="/">Volver a la Home</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Page Error  */}

                {/* Start Back To Top */}
                <div className="backto-top">
                    <ScrollToTop showUnder={160}>
                        <FiChevronUp />
                    </ScrollToTop>
                </div>
                {/* End Back To Top */}
                
                <FooterTwo />
            </div>
        )
    }
}
export default error404;
