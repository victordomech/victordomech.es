import React, { Component } from "react";

const PortfolioListContent = [
    {
        image: 'image-1',
        category: 'Clínica Dental',
        title: 'Puig & Baldrich'
    },
    {
        image: 'image-2',
        category: 'Ecommerce',
        title: 'Adarbakar'
    },
    {
        image: 'image-3',
        category: 'Plataforma',
        title: 'HolaTutor'
    },
    {
        image: 'image-4',
        category: 'Plataforma',
        title: 'Casa Rural Ripoll'
    },
    {
        image: 'image-5',
        category: 'Clínica Dental',
        title: 'Sanz'
    },
    {
        image: 'image-6',
        category: 'Taller mecánico',
        title: 'BarnapuntCar'
    }
]

class PortfolioList extends Component{
    render(){
        const {column , styevariation } = this.props;
        const list = PortfolioListContent.slice(0 , this.props.item);
        return(
            <React.Fragment> 
                {list.map((value , index) => (
                    <a href={"/project-" + value.title.replace(/\s/g, '').toLowerCase()} className={`${column}`} key={index}>
                        <div className={`portfolio ${styevariation}`}>
                            <div className="thumbnail-inner">
                                <div className={`thumbnail ${value.image}`}></div>
                                <div className={`bg-blr-image ${value.image}`}></div>
                            </div>
                            <div className="content">
                                <div className="inner">
                                    <p>{value.category}</p>
                                    <h4>{value.title}</h4>
                                    <div className="portfolio-button">
                                        <div className="rn-btn vdr_a">Ver detalles</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                ))}
               
            </React.Fragment>
        )
    }
}
export default PortfolioList;