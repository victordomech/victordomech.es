import React, { Component } from "react";
import Img from "react-webp-image";
import axios from 'axios';

class ContactThree extends Component{
    constructor(props){
        super(props);
        this.state = {
            vdrName: '',
            vdrEmail: '',
            vdrSubject: '',
            vdrMessage: '',
        };
    }

    handleSubmit(e){
        e.preventDefault();
        axios({
          method: "POST", 
          url:"https://victordomech.es/PHPMailer/mail.php", 
          data:  this.state
        }).then((response)=>{
          if (response.data.status === 'success'){
            alert("Mensaje enviado con exito."); 
            this.resetForm()
          }else if(response.data.status === 'fail'){
            alert("Mensaje fallido.")
          }
        })
      }
    
      resetForm(){
        
         this.setState({vdrName: '', vdrEmail: '', vdrSubject: '', vdrMessage: ''})
      }

    render(){
        return(
            <div className="contact-form--1">
                <div className="container">
                    <div className="row row--35 align-items-start">
                        <div className="col-lg-6 order-2 order-lg-1">
                            <div className="section-title text-left mb--50">
                                <h2 className="title">{this.props.contactTitle}</h2>
                                <p className="description">Estoy disponible para trabajar como freelancer. Contactar conmigo: <a href="tel:+34663820728">663820728</a> o email:
                                    <a href="victordomech@gmail.com">victordomech@gmail.com</a> </p>
                            </div>
                            <div className="form-wrapper">
                                <form  onSubmit={this.handleSubmit.bind(this)}> 
                                    <label htmlFor="item01">
                                        <input
                                            type="text"
                                            name="name"
                                            id="item01"
                                            value={this.state.vdrName}
                                            onChange={(e)=>{this.setState({vdrName: e.target.value});}}
                                            placeholder="Nombre *"
                                        />
                                    </label>

                                    <label htmlFor="item02">
                                        <input
                                            type="text"
                                            name="email"
                                            id="item02"
                                            value={this.state.vdrEmail}
                                            onChange={(e)=>{this.setState({vdrEmail: e.target.value});}}
                                            placeholder="Email *"
                                        />
                                    </label>

                                    <label htmlFor="item03">
                                        <input
                                            type="text"
                                            name="subject"
                                            id="item03"
                                            value={this.state.vdrSubject}
                                            onChange={(e)=>{this.setState({vdrSubject: e.target.value});}}
                                            placeholder="Asunto"
                                        />
                                    </label>
                                    <label htmlFor="item04">
                                        <textarea
                                            type="text"
                                            id="item04"
                                            name="message"
                                            value={this.state.vdrMessage}
                                            onChange={(e)=>{this.setState({vdrMessage: e.target.value});}}
                                            placeholder="Su mensaje"
                                        />
                                    </label>
                                    <button className="rn-button-style--2 btn-solid" type="submit" value="submit" name="submit" id="mc-embedded-subscribe">Enviar</button>
                                </form>
                            </div>
                        </div>
                        <div className="col-lg-6 order-1 order-lg-2">
                            <div className="thumbnail mb_md--30 mb_sm--30">
                                <Img className="w-100" src="/assets/images/victor/bannerShort.jpg" webp="/assets/images/victor/bannerShort.webp" alt="Contacto" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ContactThree;