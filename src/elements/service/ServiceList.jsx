import React ,{ Component }from "react";
import { FiLayers , FiSmartphone , FiShieldOff , FiCpu , FiLayout , FiTrendingUp } from "react-icons/fi";




const ServiceList = [
    {
        icon: <FiLayers />,
        title: 'Desarrollo web',
        description: 'Da vida a tus ideas en un navegador. Realizo páginas webs de todas las temáticas y con una gran variedad de tecnologías.'
    },
    {
        icon: <FiSmartphone />,
        title: 'Mobile first',
        description: 'Más del 90% de los usuarios visitan la web desde dispositivos móviles, me enfoco en que la experiencia de cualquier móvil sea perfecta.'
    },
    {
        icon: <FiCpu />,
        title: 'Optimización',
        description: 'Los mejores tiempos de carga están a tu alcance. Siempre desarrollo el código de manera limpia y eficiente pensando en no sobrecargar la web.'
    },
    { 
        icon: <FiLayout />,
        title: 'Código Pixel-Perfect',
        description: 'Alcanza la exactitud y perfección. Al maquetar la interfaz necesito que se ajuste exactamente al diseño.'
    },
    {
        icon: <FiShieldOff />,
        title: 'Máxima seguridad',
        description: 'Trabajo con las últimas tecnologías en seguridad, ningún ciberdelincuente podrá hacerte nada.'
    },
    { 
        icon: <FiTrendingUp />,
        title: 'Posicionamiento en buscadores',
        description: 'No te conformes con ser el segundo si puedes ser el primero. Programo siempre con las recomendaciones de Google para que no penalicen las webs.'
    }
]


class ServiceThree extends Component{
    render(){
        const {column } = this.props;
        const ServiceContent = ServiceList.slice(0 , this.props.item);
        
        return(
            <React.Fragment>
                <div className="row">
                    {ServiceContent.map( (val , i) => (
                        <div className={`${column}`} key={i}>
                            <div className="service_box">
                                <div className="service service__style--2">
                                    <div className="icon">
                                        {val.icon}
                                    </div>
                                    <div className="content">
                                        <h3 className="title">{val.title}</h3>
                                        <p>{val.description}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </React.Fragment>
        )
    }
}
export default ServiceThree;
