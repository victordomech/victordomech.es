import React, { Component } from "react";
import Helmet from "../component/common/Helmet";
import {FaTwitter ,FaInstagram , FaLinkedinIn} from "react-icons/fa";
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp } from "react-icons/fi";
import HeaderThree from "../component/header/HeaderThree";
import FooterTwo from "../component/footer/FooterTwo";

const SocialShare = [
    {Social: <FaLinkedinIn /> , link: 'https://es.linkedin.com/in/victor-domenech'},
    {Social: <FaInstagram /> , link: 'https://www.instagram.com/vdrvictor/'},
    {Social: <FaTwitter /> , link: 'https://twitter.com/domenech_epa'},
]

class PortfolioDetails extends Component{
    render(){
        return(
            <div className="active-dark">
                <React.Fragment>
                    <Helmet pageTitle="Victor Domenech Web" />

                    <HeaderThree homeLink="/" logo="symbol-dark" color="color-black"/>
                    
                    {/* Start Breadcrump Area */}
                    <div className="rn-page-title-area pt--120 pb--190 bg_image bg_image--puig"  data-black-overlay="7">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="rn-page-title text-center pt--100">
                                        <h2 className="title theme-gradient">Getting tickets to the big show</h2>
                                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* End Breadcrump Area */}

                    {/* Start Portfolio Details */}
                    <div className="rn-portfolio-details ptb--120 bg_color--1">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="portfolio-details">
                                        <div className="inner">
                                            <h2>Trydo</h2>
                                            <p className="subtitle">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commod viverra maecenas accumsan lacus vel facilisis. ut labore et dolore magna aliqua. </p>

                                            <div className="portfolio-view-list d-flex flex-wrap">
                                                <div className="port-view">
                                                    <span>Branch</span>
                                                    <h4>Ability</h4>
                                                </div>

                                                <div className="port-view">
                                                    <span>Project Types</span>
                                                    <h4>Website</h4>
                                                </div>

                                                <div className="port-view">
                                                    <span>Program</span>
                                                    <h4>View Project</h4>
                                                </div>
                                            </div>

                                            <div className="portfolio-share-link mt--20 pb--70 pb_sm--40">
                                                <ul className="social-share rn-lg-size d-flex justify-content-start liststyle mt--15">
                                                    {SocialShare.map((val , i) => (
                                                        <li key={i}><a href={`${val.link}`}>{val.Social}</a></li>
                                                    ))}
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="portfolio-thumb-inner">
                                            <div className="thumb position-relative mb--30">
                                                <img src="/assets/images/portfolio/portfolio-big-03.jpg" alt="Portfolio Images"/>
                                            </div>
                                            
                                            <div className="thumb mb--30">
                                                <img src="/assets/images/portfolio/portfolio-big-02.jpg" alt="Portfolio Images"/>
                                            </div>

                                            <div className="thumb">
                                                <img src="/assets/images/portfolio/portfolio-big-01.jpg" alt="Portfolio Images"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* End Portfolio Details */}

                    {/* Start Back To Top */}
                    <div className="backto-top">
                        <ScrollToTop showUnder={160}>
                            <FiChevronUp />
                        </ScrollToTop>
                    </div>
                    {/* End Back To Top */}
                    
                    <FooterTwo />


                </React.Fragment>
            </div>
        )
    }
}
export default PortfolioDetails;
