import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

class TabsTwo extends Component{
    render(){
        let 
        tab1 = "Mis skills",
        tab2 = "Experiencia",
        tab3 = "Educación y certificados",
        tab4 = "Aficiones";
        const { tabStyle } = this.props
        return(
            <div>
                {/* Start Tabs Area */}
                <div className="tabs-area">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <Tabs>
                                    <TabList  className={`${tabStyle}`}>
                                        <Tab>{tab1}</Tab>
                                        <Tab>{tab2}</Tab>
                                        <Tab>{tab3}</Tab>
                                        <Tab>{tab4}</Tab>
                                    </TabList>

                                    <TabPanel>
                                        <div className="single-tab-content">
                                            <ul>
                                                <li>
                                                    <span className="bold">Desarrollo web</span><span> - Wordpress, React, PHP, Sass, Javascript<br></br></span>
                                                    Desarrollo de páginas web con una gran variedad de tecnologías.
                                                </li>
                                                <li>
                                                    <span className="bold">Animaciones y efectos</span><span> - CSS y JS<br></br></span>
                                                    Curiosidad y facilidad por desarrollar nuevos efectos web que facilitan la interacción.
                                                </li>
                                                <li>
                                                    <span className="bold">Diseño de la interfaz web</span><span> - Photoshop y adobe XD<br></br></span>
                                                    Diseño de prototipos para la correcta usabilidad del usuario.
                                                </li>
                                            </ul>
                                        </div>
                                    </TabPanel>

                                    <TabPanel>
                                       <div className="single-tab-content">
                                           <ul>
                                                <li>
                                                    <span className="bold">Freelance</span><br></br>
                                                    2019-actualidad
                                               </li>
                                               <li>
                                                    <span className="bold">Desarrollador web</span><span> - Consultoría de marketing online Imas3<br></br></span>
                                                    2017-actualidad
                                               </li>
                                               <li>
                                                    <span className="bold">Programador</span><span> - Deideas Marketing<br></br></span>
                                                    2016-2017
                                               </li>
                                           </ul>
                                       </div>
                                    </TabPanel>



                                    <TabPanel>
                                       <div className="single-tab-content">
                                           <ul>
                                               <li>
                                                    <span className="bold">Grado Ingeniería Informática</span><span> - UPF<br></br></span>2020
                                               </li>
                                               <li>
                                                   <span className="bold">Diseño de sitios móviles</span><span> - Google<br></br></span>2019
                                               </li>
                                               <li>
                                                   <span className="bold">Desarrollo Web: HTML y CSS</span><span> - Google<br></br></span>2019
                                               </li>
                                               <li>
                                                   <span className="bold">Curso de React JS</span><span> - Imagina Group<br></br></span>2018
                                               </li>
                                               <li>
                                                   <span className="bold">Curso desarrollo Aplicaciones web</span><span> - Fundación CIP<br></br></span>2016
                                               </li>
                                           </ul>
                                       </div>
                                    </TabPanel>

                                    <TabPanel>
                                       <div className="single-tab-content">
                                           <ul>
                                                <li>
                                                    <span className="bold">Jugar al baloncesto</span><br></br>No hay nada mejor para despejarse que unas canastas con amigos.
                                               </li>
                                               <li>
                                                   <span className="bold">Las alturas</span><br></br>Me encanta saltar en paracaídas o hacer puenting.
                                               </li> 
                                               <li>
                                                   <span className="bold">YouTube</span><br></br>Me encanta estar enterado de todo lo nuevo, YouTube es mi mejor herramienta.
                                               </li>
                                           </ul>
                                       </div>
                                    </TabPanel>
                                    
                                </Tabs>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Tabs Area */}
            </div>
        )
    }
}



export default TabsTwo;