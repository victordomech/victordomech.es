// React Required
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Create Import File
import './index.scss';

// Common Layout
// import Layout from "./component/common/App";


// Home layout
import Demo from './dark/PortfolioLanding';

import error404 from "./elements/error404";
// Blocks Layout

import ProjectPuig from "./projects/puig&baldrich";
import ProjectAdarbakar from "./projects/adarbakar";
import ProjectHolaTutor from "./projects/holatutor";
import ProjectCasaRuralRipoll from "./projects/casaruralripoll";
import ProjectSanz from "./projects/sanz";
import ProjectBarnaPuntCar from "./projects/barnapuntcar";

import { BrowserRouter, Switch, Route  } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import ReactGA from 'react-ga';

ReactGA.initialize('UA-168683553-1');
ReactGA.pageview(window.location.pathname + window.location.search);


class Root extends Component{
    render(){
        return(
            <BrowserRouter basename={'/'}>
                <Switch>
                    <Route exact path={`${process.env.PUBLIC_URL}/`} component={Demo}/>

                    {/* Projects  */}
                    <Route exact path={`${process.env.PUBLIC_URL}/project-puig&baldrich`} component={ProjectPuig}/>
                    <Route exact path={`${process.env.PUBLIC_URL}/project-adarbakar`} component={ProjectAdarbakar}/>
                    <Route exact path={`${process.env.PUBLIC_URL}/project-holatutor`} component={ProjectHolaTutor}/>
                    <Route exact path={`${process.env.PUBLIC_URL}/project-casaruralripoll`} component={ProjectCasaRuralRipoll}/>
                    <Route exact path={`${process.env.PUBLIC_URL}/project-sanz`} component={ProjectSanz}/>
                    <Route exact path={`${process.env.PUBLIC_URL}/project-barnapuntcar`} component={ProjectBarnaPuntCar}/>

                    <Route path={`${process.env.PUBLIC_URL}/404`} component={error404}/>
                    <Route component={error404}/>

                </Switch>
            </BrowserRouter>
        )
    }
}

ReactDOM.render(<Root/>, document.getElementById('root'));
serviceWorker.register();