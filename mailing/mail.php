<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
//SPF ANTERIOR v=spf1 +a +mx +a:<hostname> -all
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
header("Content-Type: application/json");

$rest_json = file_get_contents("php://input");
$_POST = json_decode($rest_json, true);

$errors = array();
if ($_SERVER['REQUEST_METHOD'] === "POST") {

    if (empty($_POST['vdrEmail'])) {
        $errors[] = 'Email is empty';
    } else {
        $email = $_POST['vdrEmail'];
        
        // validating the email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = 'Invalid email';
        }
    }
    if (empty($_POST['vdrName'])) {
        $errors[] = 'Name is empty';
    } else {
        $name = $_POST['vdrName'];
    }

    if (empty($_POST['vdrSubject'])) {
        $errors[] = 'Subject is empty';
    } else {
        $subj = $_POST['vdrSubject'];
    }

    if (empty($_POST['vdrMessage'])) {
        $errors[] = 'Message is empty';
    } else {
        $message = $_POST['vdrMessage'];
    }

    if (empty($errors)) {
        $date = date('j, F Y h:i A');
        
        $msg = "
            <html>
            <head>
            <title>$name intenta contactar</title>
            </head>
            <body style=\"background-color:#fafafa;\">
            <div style=\"padding:20px;\">
            <br>
            Nombre: <span style=\"color:#888\">$name</span>
            <br>
            Mail: <span style=\"color:#888\">$email</span>
            <br>
            Mensaje: <div style=\"color:#888\">$message</div>
            <br>
            Fecha: <span style=\"color:#888\">$date</span>
            </div>
            </body>
            </html>
        ";

        $to ='hello@victordomech.es';
        $from = $email;

        $error=smtpmailer($to,$from, $name ,$subj, $msg);
    }
}

function smtpmailer($to, $from, $from_name, $subject, $body){
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPAuth = true; 

    $mail->SMTPSecure = 'ssl'; 
    $mail->Host = 'mail.victordomech.es';
    $mail->Port = 465;  
    $mail->Username = 'hello@victordomech.es';
    $mail->Password = '1VictorDomenech_';   

//   $path = 'reseller.pdf';
//   $mail->AddAttachment($path);

    $mail->IsHTML(true);
    $mail->From= $from;
    $mail->FromName= $from_name;
    $mail->Sender= $from;
    $mail->AddReplyTo($from, $from_name);
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->AddAddress($to);
    if(!$mail->Send())
    {?> 
        {
        "status": "fail",
        "error":  <?php echo json_encode($errors) ?>
        }
    <?php }
    else 
    {?> {
          "status": "success",
          "message": "Your data was successfully submitted"
        }
    <?php }
}
    
?>
